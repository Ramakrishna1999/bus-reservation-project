package com.admin.services.exceptions;


public class Constants {
	public static final String FAILED = "Not found with given values";
	public static final String BUS_NOT_FOUND = " Bus Not Found" ;
	public static final String STUDENT_NOT_FOUND =" User Not Found";
//	public static final String REQUESTBOOK_NOT_FOUND = "Request Book Not Found";
	public static final int JWT_TOKEN_VALIDITY =30 * 60;
	public static final String VALIDATION_FAILED = "Validation Failed";
	public static final String USER_DETAILS_NOTAVAILABLE = "Entered User details are not Available";
	
	
}