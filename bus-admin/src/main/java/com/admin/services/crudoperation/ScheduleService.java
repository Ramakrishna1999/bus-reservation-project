package com.admin.services.crudoperation;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.admin.services.dao.ScheduleDao;
import com.admin.services.models.CheckDateFormat;
import com.admin.services.models.Schedule;

@Service
public class ScheduleService {

	@Autowired
	private ScheduleDao scheduledao;

	public Schedule createSchedule(Schedule schedule) {
		String date = CheckDateFormat.checkDate(schedule.getDateOfJourney());
		boolean startStatus = CheckDateFormat.isValidTime(schedule.getDateOfJourney());
		boolean endStatus = CheckDateFormat.isValidTime(schedule.getDateOfJourney());
		if (date.equals(schedule.getDateOfJourney()) && startStatus == true && endStatus == true) {
			scheduledao.save(schedule);
		} else {
			scheduledao.save(null);
		}
		return schedule;
	}

	public Schedule modifytime(int id, Schedule schedule) {

		if (schedule.getId() == id) {
			scheduledao.save(schedule);
		} else {
			scheduledao.save(null);
		}
		return schedule;
	}

	public String deleteSchedule(int id) {

		scheduledao.deleteById(id);
		return "Deleted Successfully";
	}

	public Schedule findSchedule(String id) {
		return scheduledao.findByDateOfJourney(id);
	}

	public List<Schedule> findSchedules() {
		return scheduledao.findAll();
	}

	public Schedule findScheduleById(int id) {
		return scheduledao.findById(id);
	}

}
