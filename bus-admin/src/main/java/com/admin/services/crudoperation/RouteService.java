package com.admin.services.crudoperation;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.admin.services.dao.RouteDao;
import com.admin.services.models.Route;

@Service
public class RouteService {

	@Autowired
	private RouteDao routedao;

	public Route createRoute(Route route) {
		return routedao.save(route);
	}

	public Route updateRoute(int id, Route route) {

		if (route.getId() == id) {
			routedao.save(route);
		} else {
			routedao.save(null);
		}
		return route;
	}

	public String deleteRoute(int id) {
		routedao.deleteById(id);
		return "Deleted One route Succesfully";
	}

	public Route findRoute(int id) {
		return routedao.findById(id);

	}

	public List<Route> findRoutes() {
		return routedao.findAll();
	}

}
