package com.admin.services.crudoperation;

import org.springframework.beans.factory.annotation.Autowired;

import com.admin.services.dao.MemberDao;
import com.admin.services.models.Member;

public class AdminService {

	@Autowired
	MemberDao memberdao;
	
	public Member createAdmin(Member member) {
	return	memberdao.save(member);
	}
}
