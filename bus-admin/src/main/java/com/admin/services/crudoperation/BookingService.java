package com.admin.services.crudoperation;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.admin.services.dao.BookingDao;
import com.admin.services.models.BookingDetails;

@Service
public class BookingService {
	@Autowired
	BookingDao bookingdao;

	public BookingDetails findTicket(long id) {
		return bookingdao.findByBookingId(id);
	}

	public List<BookingDetails> findAllTickets() {
		return bookingdao.findAll();
	}

}
