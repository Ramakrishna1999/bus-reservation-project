package com.admin.services.crudoperation;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.admin.services.dao.BusDao;
import com.admin.services.models.Bus;

@Service
public class BusService {

	@Autowired
	private BusDao busdao;

	public Bus createBus(Bus bus) {
		return busdao.save(bus);
	}

	public Bus modifyBusDetails(int id, Bus bus) {

		if (bus.getId() == id) {
			busdao.save(bus);
		} else {
			busdao.save(null);
		}

		return bus;
	}

	public String deleteBus(int id) {
		busdao.deleteById(id);
		return "Deleted Successfully";
	}

	public Bus findBusDetails(int id) {
		return busdao.findById(id);
	}

	public List<Bus> findAllBusDetails() {
		return busdao.findAll();
	}
}
