package com.admin.services;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import springfox.documentation.swagger2.annotations.EnableSwagger2;


@SpringBootApplication
@EnableSwagger2
public class BusAdminApplication {

	public static void main(String[] args) {
		SpringApplication.run(BusAdminApplication.class, args);
		
	}

//	@Bean
//	public Docket swaggerConfiguration() {
//		// return a prepared Docket Instance
//		return new Docket(DocumentationType.SWAGGER_2).select() // ApiSelectorBuilder
//				.paths(PathSelectors.ant("/**")).apis(RequestHandlerSelectors.basePackage("com.admin.services"))
//				.build().apiInfo(apiDetailsOfAdmin());
//
//	}
//
//	
//	
//	private ApiInfo apiDetailsOfAdmin() {
//		return new ApiInfo("BUS Services REST API",
//				"BOOKING  REST API.", "1.0", "Terms of service",
//				new Contact("PENTI RAMAKRISHNA", "", "ram@gmail.com"), "License of API", "API license URL",
//				Collections.emptyList());
//	}
}
