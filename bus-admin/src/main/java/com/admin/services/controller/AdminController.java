package com.admin.services.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.admin.services.crudoperation.AdminService;
import com.admin.services.models.Member;

@RestController
@RequestMapping("/admin")
public class AdminController {

	@Autowired
	AdminService adminservice;
	public Member registerAdmin(@RequestBody Member member) {
		return adminservice.createAdmin(member);
		
	}
}
