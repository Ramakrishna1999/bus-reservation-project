package com.admin.services.controller;

import java.util.List;


import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.admin.services.crudoperation.BookingService;
import com.admin.services.models.BookingDetails;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping("/bookingAPI")
public class BookingController {

	@Autowired
	BookingService bookingservice;

	@ApiOperation(value = "FIND ONE PASSENGER BOOKING INFORMATION")
	@ApiResponses(value = {
			@ApiResponse(code = HttpServletResponse.SC_OK, response = BookingDetails.class, message = " Booking Details Fetched Successfully"),
			@ApiResponse(code = HttpServletResponse.SC_BAD_REQUEST, response = String.class, message = "Invalid parameters"),
			@ApiResponse(code = HttpServletResponse.SC_UNAUTHORIZED, response = String.class, message = "Invalid Token / Without Token"),
			@ApiResponse(code = HttpServletResponse.SC_FORBIDDEN, response = String.class, message = "UnAuthorized Access") })
	@GetMapping(value = "/fetchticket/{bookingid}")
	public BookingDetails findBookingDetails(@PathVariable long bookingid) {

		return bookingservice.findTicket(bookingid);
	}

	@ApiOperation(value = "FIND ALL PASSENGER BOOKING INFORMATION")
	@ApiResponses(value = {
			@ApiResponse(code = HttpServletResponse.SC_OK, response = BookingDetails.class, message = " Booking Details Fetched Successfully"),
			@ApiResponse(code = HttpServletResponse.SC_BAD_REQUEST, response = String.class, message = "Invalid parameters"),
			@ApiResponse(code = HttpServletResponse.SC_UNAUTHORIZED, response = String.class, message = "Invalid Token / Without Token"),
			@ApiResponse(code = HttpServletResponse.SC_FORBIDDEN, response = String.class, message = "UnAuthorized Access") })
	@GetMapping(value = "/fetchingalltickets")
	public List<BookingDetails> findAllPassengerBookingDetails() {

		return bookingservice.findAllTickets();
	}

}
