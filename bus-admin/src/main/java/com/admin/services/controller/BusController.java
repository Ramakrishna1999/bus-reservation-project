package com.admin.services.controller;

import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.admin.services.crudoperation.BusService;
import com.admin.services.models.Bus;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping("/bus")
public class BusController {

	
	
	
	@Autowired
	private BusService busService;
	
	@ApiOperation(value = "ADDING BUS TO LIST")
	@ApiResponses(value= {
	  @ApiResponse(code = HttpServletResponse.SC_OK, response = String.class, message = " Bus Details sent Successfully"),
	  @ApiResponse(code = HttpServletResponse.SC_BAD_REQUEST, response = String.class, message = "Invalid parameters"),
	  @ApiResponse(code = HttpServletResponse.SC_UNAUTHORIZED, response = String.class, message = "Invalid Token / Without Token"),
	  @ApiResponse(code = HttpServletResponse.SC_FORBIDDEN, response = String.class, message = "UnAuthorized Access")  })
	@PostMapping(value = "/create")
	public Bus addBus( @RequestBody Bus bus) {
		return busService.createBus(bus);
	}

	@ApiOperation(value = "UPDATING BUS DETAILS")
	@ApiResponses(value= {
	  @ApiResponse(code = HttpServletResponse.SC_OK, response = String.class, message = "Bus details are modified succesfully"),
	  @ApiResponse(code = HttpServletResponse.SC_BAD_REQUEST, response = String.class, message = "Invalid parameters"),
	  @ApiResponse(code = HttpServletResponse.SC_UNAUTHORIZED, response = String.class, message = "Invalid Token / Without Token"),
	  @ApiResponse(code = HttpServletResponse.SC_FORBIDDEN, response = String.class, message = "UnAuthorized Access")  })
	@PutMapping(value = "/update/{id}")
	public Bus updateBus(@Validated @PathVariable("id") int id, @RequestBody Bus bus) {
		return busService.modifyBusDetails(id,bus);
	}
	
	@ApiOperation(value = "REMOVING BUS FROM LIST")
	@ApiResponses(value= {
			  @ApiResponse(code = HttpServletResponse.SC_OK, response = String.class, message = "Bus details are dodified succesfully"),
			  @ApiResponse(code = HttpServletResponse.SC_BAD_REQUEST, response = String.class, message = "enter valid busid parameter"),
			  @ApiResponse(code = HttpServletResponse.SC_UNAUTHORIZED, response = String.class, message = "Invalid Token / Without Token"),
			  @ApiResponse(code = HttpServletResponse.SC_FORBIDDEN, response = String.class, message = "UnAuthorized Access")  })	
	@DeleteMapping(value = "/delete/{id}")
	public String deleteOneBus(@Validated @PathVariable int  id) {
		return busService.deleteBus(id);
	}
	
	@ApiOperation(value = "GET ONE BUS FROM LIST")	
	@ApiResponses(value= {
			  @ApiResponse(code = HttpServletResponse.SC_OK, response = Bus.class, message = "Bus details are retrived succesfully"),
			  @ApiResponse(code = HttpServletResponse.SC_BAD_REQUEST, response = String.class, message = "enter valid busid parameter"),
			  @ApiResponse(code = HttpServletResponse.SC_UNAUTHORIZED, response = String.class, message = ""),
			  @ApiResponse(code = HttpServletResponse.SC_FORBIDDEN, response = String.class, message = "UnAuthorized Access")  })
	@GetMapping(value = "/find/{id}")
	public Bus getOneBus(@Validated @PathVariable("id") int id) {
		return busService.findBusDetails(id);
	}

	@ApiOperation(value = "FIND ALL BUS DETAILS")
	@ApiResponses(value= {
			  @ApiResponse(code = HttpServletResponse.SC_OK, response = Bus.class, message = " All bus details are retrived succesfully"),
			  @ApiResponse(code = HttpServletResponse.SC_BAD_REQUEST, response = String.class, message = "No need to enter any parrameter"),
			  @ApiResponse(code = HttpServletResponse.SC_UNAUTHORIZED, response = String.class, message = "Invalid Token / Without Token"),
			  @ApiResponse(code = HttpServletResponse.SC_FORBIDDEN, response = String.class, message = "UnAuthorized Access")  })
	@GetMapping(value = "/listOfBuses")
	public List<Bus> getAllBus() {
		return busService.findAllBusDetails();
	}
	
}
