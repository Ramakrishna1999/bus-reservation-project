package com.admin.services.controller;

import java.util.List;


import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.admin.services.crudoperation.RouteService;
import com.admin.services.models.Route;

import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping("/routesInformation")
public class RouteController {

	@Autowired
	RouteService routeservice;

	@PostMapping(value = "/create/route")
	@ApiResponses(value= {
			  @ApiResponse(code = HttpServletResponse.SC_OK, response = String.class, message = "Route details are created succesfully"),
			  @ApiResponse(code = HttpServletResponse.SC_BAD_REQUEST, response = String.class, message = "Invalid parameters"),
			  @ApiResponse(code = HttpServletResponse.SC_UNAUTHORIZED, response = String.class, message = "Invalid Token / Without Token"),
			  @ApiResponse(code = HttpServletResponse.SC_FORBIDDEN, response = String.class, message = "UnAuthorized Access")  })
	public Route createRoute(@Validated @RequestBody Route route) {
		return routeservice.createRoute(route);
	}

	@ApiResponses(value= {
			  @ApiResponse(code = HttpServletResponse.SC_OK, response = String.class, message = "Route details are modified succesfully"),
			  @ApiResponse(code = HttpServletResponse.SC_BAD_REQUEST, response = String.class, message = "Invalid parameters"),
			  @ApiResponse(code = HttpServletResponse.SC_UNAUTHORIZED, response = String.class, message = "Invalid Token / Without Token"),
			  @ApiResponse(code = HttpServletResponse.SC_FORBIDDEN, response = String.class, message = "UnAuthorized Access")  })
	@PutMapping(value = "/updateRoute/{id}")
	public Route modifyRoute(@Validated @PathVariable int id,@RequestBody Route route) {
		return routeservice.updateRoute(id,route);
	}

	@DeleteMapping(value = "/delete/{id}")
	@ApiResponses(value= {
			  @ApiResponse(code = HttpServletResponse.SC_OK, response = String.class, message = "Route details are deleted succesfully"),
			  @ApiResponse(code = HttpServletResponse.SC_BAD_REQUEST, response = String.class, message = "Invalid parameters"),
			  @ApiResponse(code = HttpServletResponse.SC_UNAUTHORIZED, response = String.class, message = "Invalid Token / Without Token"),
			  @ApiResponse(code = HttpServletResponse.SC_FORBIDDEN, response = String.class, message = "UnAuthorized Access")  })
	public String deleteOneRoute(@Validated @PathVariable int id) {
		return routeservice.deleteRoute(id);
	}

	@GetMapping(value = "/retrive/{id}")
	@ApiResponses(value= {
			  @ApiResponse(code = HttpServletResponse.SC_OK, response = String.class, message = "Route details are modified succesfully"),
			  @ApiResponse(code = HttpServletResponse.SC_BAD_REQUEST, response = String.class, message = "Invalid parameters"),
			  @ApiResponse(code = HttpServletResponse.SC_UNAUTHORIZED, response = String.class, message = "Invalid Token / Without Token"),
			  @ApiResponse(code = HttpServletResponse.SC_FORBIDDEN, response = String.class, message = "UnAuthorized Access")  })
	public Route getOneRoute(@Validated @PathVariable int id) {
		return routeservice.findRoute(id);
	}
	@ApiResponses(value= {
			  @ApiResponse(code = HttpServletResponse.SC_OK, response = Route.class, message = "Route details are modified succesfully"),
			  @ApiResponse(code = HttpServletResponse.SC_BAD_REQUEST, response = String.class, message = "Invalid parameters"),
			  @ApiResponse(code = HttpServletResponse.SC_UNAUTHORIZED, response = String.class, message = "Invalid Token / Without Token"),
			  @ApiResponse(code = HttpServletResponse.SC_FORBIDDEN, response = String.class, message = "UnAuthorized Access")  })
	@GetMapping(value = "/listOfRoutes")
	public List<Route> getAllRoute() {
		return routeservice.findRoutes();
	}
	

}
