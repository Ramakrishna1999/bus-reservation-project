package com.admin.services.models;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Route {

	
//	private static final Logger log = LoggerFactory.getLogger(AccountService.class);

	@Id
	//@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;
	private long routeno;
	private String source; 
	private String destination;	
	private long cost;
	public Route() {
		super();
		
	}
	
	/**
	 * @return the bus
	 */
	
	
	/**
	 * @return the destination
	 */
	public String getDestination() {
		return destination;
	}
	/**
	 * @return the routeno
	 */
	/**
	 * @return the routeno
	 */
	public long getRouteno() {
		return routeno;
	}
	/**
	 * @return the source
	 */
	public String getSource() {
		return source;
	}
	
	
	
	
	/**
	 * @param bus the bus to set
	 */
	
	/**
	 * @param destination the destination to set
	 */
	public void setDestination(String destination) {
		this.destination = destination;
	}
	/**
	 * @param routeno the routeno to set
	 */
	public void setRouteno(long routeno) {
		this.routeno = routeno;
	}
	/**
	 * @param source the source to set
	 */
	public void setSource(String source) {
		this.source = source;
	}
	
	

	

	/**
	 * @return the cost
	 */
	public long getCost() {
		return cost;
	}

	/**
	 * @param cost the cost to set
	 */
	public void setCost(long cost) {
		this.cost = cost;
	}

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	public Route(int id, long routeno, String source, String destination, long cost) {
		super();
		this.id = id;
		this.routeno = routeno;
		this.source = source;
		this.destination = destination;
		this.cost = cost;
	}

	@Override
	public String toString() {
		return "Route [id=" + id + ", routeno=" + routeno + ", source=" + source + ", destination=" + destination
				+ ", cost=" + cost + "]";
	}

	
	
	
	
}
