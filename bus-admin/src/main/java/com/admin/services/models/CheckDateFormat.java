package com.admin.services.models;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CheckDateFormat {

	static int MAX_VALID_YR = 9999;
	static int MIN_VALID_YR = 1800;

	// Returns true if
	// given year is valid.
	static boolean isLeap(long year) {
		// Return true if year is
		// a multiple of 4 and not
		// multiple of 100.
		// OR year is multiple of 400.
		return (((year % 4 == 0) && (year % 100 != 0)) || (year % 400 == 0));
	}

	// Returns true if given
	// year is valid or not.
	static boolean isValidDate(int d, int m, int y) {
		// If year, month and day
		// are not in given range
		if (y > MAX_VALID_YR || y < MIN_VALID_YR)
			return false;
		if (m < 01 || m > 12)
			return false;
		if (d < 01 || d > 31)
			return false;

		// Handle February month
		// with leap year
		if (m == 2) {
			if (isLeap(y))
				return (d <= 29);
			else
				return (d <= 28);
		}

		// Months of April, June,
		// Sept and Nov must have
		// number of days less than
		// or equal to 30.
		if (m == 04 || m == 06 || m == 9 || m == 11)
			return (d <= 30);

		return true;
	}

	// Driver code
	public static String checkDate(String date) {
		String original;
		String[] a1 = date.split("-");
		int[] b = new int[a1.length];
		for (int i = 0; i < a1.length; i++) {
			int j = Integer.parseInt(a1[i]);
			b[(int) i] = j;
		}
		int day = b[0];
		int month = b[1];
		int year = b[2];

		if (isValidDate(day, month, year) == true) {
			String day1, month1;

			if (day < 10) {
				day1 = 0 + "" + day;
			} else {
				day1 = "" + day;
			}
			if (month < 10) {
				month1 = 0 + "" + month;
			} else {
				month1 = "" + month;
			}

			original = day1 + "-" + month1 + "-" + year;
		} else {
			original = null;
		}
		return original;
	}

	public static void main(String[] args) {
		String str1 = "23:59:59";
		System.out.println(isValidTime(str1));
	}

	// Function to validate the time in 24-hour format
	public static boolean isValidTime(String time) {

		// Regex to check valid time in 24-hour format.
		String regex = "([01]?[0-9]|2[0-3]):[0-5][0-9]:[0-5][0-9]";

		// Compile the ReGex
		Pattern p = Pattern.compile(regex);

		// If the time is empty
		// return false
		if (time == null) {
			return false;
		}

		// Pattern class contains matcher() method
		// to find matching between given time
		// and regular expression.
		Matcher m = p.matcher(time);

		// Return if the time
		// matched the ReGex
		return m.matches();
	}

	// Driver Code.

}
