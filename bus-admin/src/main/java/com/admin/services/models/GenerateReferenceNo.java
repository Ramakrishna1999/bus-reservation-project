package com.admin.services.models;

import java.util.Random;

public class GenerateReferenceNo {

	public static long generateRef() {
		
		Random random=new Random();
		long limit=random.nextInt(1000000);
		if(limit<0) {
			limit=limit*-1;
		}
		if(limit<10) {
			limit=limit+100000;
		}
		if(limit<100) {
			limit=limit+10000;
		}
		if(limit<1000) {
			limit=limit+1000;
		}
		if(limit<10000) {
			limit=limit+100;
		}
		if(limit<100000) {
			limit=limit+10;
		}
		return limit;
	}
}
