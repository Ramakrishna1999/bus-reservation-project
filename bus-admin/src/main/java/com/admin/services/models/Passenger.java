package com.admin.services.models;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class Passenger {

	@Id
	private int id;
	private int passengerid;
	private String name;
	private String gender;
	private int age;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "Member_id")
	Member member;

	/**
	 * @return the name
	 */

	public String getName() {
		return name;
	}

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	public Passenger() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @return the gender
	 */
	public String getGender() {
		return gender;
	}

	/**
	 * @param gender the gender to set
	 */
	public void setGender(String gender) {
		this.gender = gender;
	}

	/**
	 * @return the age
	 */
	public int getAge() {
		return age;
	}

	/**
	 * @param age the age to set
	 */
	public void setAge(int age) {
		this.age = age;
	}

	/**
	 * @return the passengerid
	 */
	public int getPassengerid() {
		return passengerid;
	}

	/**
	 * @param passengerid the passengerid to set
	 */
	public void setPassengerid(int passengerid) {
		this.passengerid = passengerid;
	}

	public Passenger(int id, int passengerid, String name, String gender, int age, Member member) {
		super();
		this.id = id;
		this.passengerid = passengerid;
		this.name = name;
		this.gender = gender;
		this.age = age;
		this.member = member;
	}

	public Member getMember() {
		return member;
	}

	public void setMember(Member member) {
		this.member = member;
	}

	@Override
	public String toString() {
		return "Passenger [id=" + id + ", passengerid=" + passengerid + ", name=" + name + ", gender=" + gender
				+ ", age=" + age + ", member=" + member + "]";
	}

	/**
	 * @return the contact
	 */
	

}
