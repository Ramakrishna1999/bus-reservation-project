package com.admin.services.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

@Entity
public class Bus {

	@Id
	private int id;
	private String busno;
	private String bustype;
	@Column(name = "availableseats")
	private int availableSeats;
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "route_id")
	private Route route;
	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "schedule_id")
	private Schedule schedule;

	/**
	 * @return the availableSeats
	 */
	public int getAvailableSeats() {
		return availableSeats;
	}

	/**
	 * @param availableSeats the availableSeats to set
	 */
	public void setAvailableSeats(int availableSeats) {
		this.availableSeats = availableSeats;
	}

	/**
	 * @return the busno
	 */
	public String getBusno() {
		return busno;
	}

	/**
	 * @param busno the busno to set
	 */
	public void setBusno(String busno) {
		this.busno = busno;
	}

	/**
	 * @return the bustype
	 */
	public String getBustype() {
		return bustype;
	}

	/**
	 * @param bustype the bustype to set
	 */
	public void setBustype(String bustype) {
		this.bustype = bustype;
	}

	/**
	 * @return the locations
	 */
	public Bus() {
		super();
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Route getRoute() {
		return route;
	}

	public void setRoute(Route route) {
		this.route = route;
	}

	public Schedule getSchedule() {
		return schedule;
	}

	public void setSchedule(Schedule schedule) {
		this.schedule = schedule;
	}

	public Bus(int id, String busno, String bustype, int availableSeats, Route route, Schedule schedule) {
		super();
		this.id = id;
		this.busno = busno;
		this.bustype = bustype;
		this.availableSeats = availableSeats;
		this.route = route;
		this.schedule = schedule;
	}

	@Override
	public String toString() {
		return "Bus [id=" + id + ", busno=" + busno + ", bustype=" + bustype + ", availableSeats=" + availableSeats
				+ ", route=" + route + ", schedule=" + schedule + "]";
	}

}
