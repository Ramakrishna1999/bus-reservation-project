package com.admin.services.models;

import javax.persistence.Entity;
import javax.persistence.Id;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonFormat.Shape;

@Entity
public class Schedule {


	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "HH:mm:ss")
	private String startTime;


	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "HH:mm:ss")
	private String endTime;

	@JsonFormat(pattern = "dd-MM-yyyy", shape = Shape.STRING)
	private String dateOfJourney;
	@Id
	private int id;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDateOfJourney() {
		return dateOfJourney;
	}

	public void setDateOfJourney(String dateOfJourney) {
		this.dateOfJourney = dateOfJourney;
	}

	public String getStartTime() {
		return startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	public String getEndTime() {
		return endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	@Override
	public String toString() {
		return "Schedule [startTime=" + startTime + ", endTime=" + endTime + ", dateOfJourney=" + dateOfJourney
				+ ", id=" + id + "]";
	}

	public Schedule(String startTime, String endTime, String dateOfJourney, int id) {
		super();
		this.startTime = startTime;
		this.endTime = endTime;
		this.dateOfJourney = dateOfJourney;
		this.id = id;
	}

	public Schedule() {
		super();
		// TODO Auto-generated constructor stub
	}

	

}
