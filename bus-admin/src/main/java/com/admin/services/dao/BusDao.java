package com.admin.services.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.admin.services.models.Bus;

@Repository
public interface BusDao extends JpaRepository<Bus, Integer> {

	Bus findById(int id);

	List<Bus> findAll();
	// Bus save(Bus bus);

}
