package com.admin.services.dao;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.admin.services.models.Member;

public interface MemberDao extends JpaRepository<Member, Long>{

	Optional<Member> findByName(String name);
	Member findByEmail(String email);
}
