package com.admin.services.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.admin.services.models.Member;
import com.admin.services.models.Passenger;
@Repository
public interface PassengerDao extends JpaRepository<Passenger, Integer> {

	Passenger findById(int passengerid);
	
	Passenger findByName(String name);
	
	List<Passenger> findByMember(Member name);
}
