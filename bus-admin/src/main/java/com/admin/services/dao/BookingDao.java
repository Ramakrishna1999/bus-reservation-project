package com.admin.services.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import org.springframework.stereotype.Repository;

import com.admin.services.models.BookingDetails;
@Repository
public interface BookingDao extends JpaRepository<BookingDetails, Long> {

	BookingDetails findByBookingId(long id);
}
