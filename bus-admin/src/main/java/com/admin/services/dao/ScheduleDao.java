package com.admin.services.dao;

import java.util.List;


import org.springframework.data.jpa.repository.JpaRepository;

import com.admin.services.models.Schedule;

public interface ScheduleDao extends JpaRepository<Schedule, Integer> {

	Schedule findById(int id);
	List<Schedule> findAll();
	Schedule findByDateOfJourney(String dateOfJourney);
}
