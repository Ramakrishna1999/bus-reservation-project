package com.admin.services.dao;

import java.util.List;


import org.springframework.data.jpa.repository.JpaRepository;

import com.admin.services.models.Route;

public interface RouteDao extends JpaRepository<Route,Integer>{

	List<Route> findBySource(String source);
	
	List<Route> findAll();
	Route findById(int id);


}
