package com.bus.services.testservices;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;

import com.admin.services.crudoperation.BusService;
import com.admin.services.dao.BusDao;
import com.admin.services.models.Bus;
import com.admin.services.models.Route;
import com.admin.services.models.Schedule;

@TestMethodOrder(OrderAnnotation.class)
@SpringBootTest(classes = { BusServiceMockitoTest.class })
public class BusServiceMockitoTest {

	@Mock
	BusDao busdao;

	@InjectMocks
	BusService busservice;
	Schedule schedule = new Schedule("09:00:00", "08:00:00", "01-01-2022", 1);

	Route route = new Route(1, 111l, "Choppadandi", "Manchiryal", 150l);

	Bus bus = new Bus(1, "Ts0199918", "express", 2, route, schedule);

	@Test
	@Order(1)
	public void test_getFindBuses() {
		List<Bus> busesForFind = new ArrayList<Bus>();
		busesForFind.add(new Bus(1, "Ts0199918", "express", 2, route, schedule));
		busesForFind.add(new Bus(1, "Ts0199918", "express", 2, route, schedule));
		when(busdao.findAll()).thenReturn(busesForFind);
		assertEquals(busesForFind, busservice.findAllBusDetails());
	}

	// test for find single scheudle
	@Test
	@Order(2)
	public void test_getFindById() {

		int id = 1;
		when(busdao.findById(id)).thenReturn(bus);
		assertEquals(id, busservice.findBusDetails(id).getId());

	}

	@Test
	@Order(3)
	public void test_CreateBus() {

		when(busdao.save(bus)).thenReturn(bus);
		assertEquals(bus, busservice.createBus(bus));

	}

	@Test
	@Order(4)
	public void test_UpdateBus() {

		int provideID = 1;
		when(busservice.modifyBusDetails(provideID, bus)).thenReturn(bus);
		assertEquals(provideID, busservice.modifyBusDetails(provideID, bus).getId());

	}

	@Test
	@Order(5)
	public void test_DeleteBus() {

		int provideID = 1;
		busservice.deleteBus(provideID);
		verify(busdao, times(1)).deleteById(provideID);

	}
}
