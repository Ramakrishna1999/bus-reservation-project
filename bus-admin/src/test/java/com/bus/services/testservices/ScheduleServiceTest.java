package com.bus.services.testservices;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;

import com.admin.services.crudoperation.ScheduleService;
import com.admin.services.dao.ScheduleDao;
import com.admin.services.models.Route;
import com.admin.services.models.Schedule;

@TestMethodOrder(OrderAnnotation.class)
@SpringBootTest(classes = { ScheduleServiceMockitoTest.class })
public class ScheduleServiceMockitoTest {

	@Mock
	ScheduleDao scheduledao;

	@InjectMocks
	ScheduleService scheduleservices;

	public List<Schedule> schedule;

	public Schedule oneSchedule = new Schedule("09:00:00", "08:00:00",
			"01-01-2022", 1);
	@Test
	@Order(1)
	public void test_getFindSchedules() {
		schedule = new ArrayList<Schedule>();
		schedule.add( new Schedule("09:00:00", "08:00:00",
				"01-01-2022", 1));
		when(scheduleservices.findSchedules()).thenReturn(schedule);
		assertEquals(schedule, scheduledao.findAll());
	}

	// test for find single scheudle
	@Test
	@Order(2)
	public void test_getFindById() {
		
		int id = 1;
		when(scheduledao.findById(id)).thenReturn(oneSchedule);
		System.out.println(oneSchedule.getId());
		assertEquals(id, scheduleservices.findScheduleById(id).getId());

	}

	@Test
	@Order(3)
	public void test_CreateSchedule() {
	
		when(scheduledao.save(oneSchedule)).thenReturn(oneSchedule);
		assertEquals(oneSchedule, scheduleservices.createSchedule(oneSchedule));

	}
	
	@Test
	@Order(4)
	public void test_UpdateSchedule() {
		
		int provideID = 1;
		when(scheduledao.save(oneSchedule)).thenReturn(oneSchedule);
		assertEquals(provideID, scheduleservices.modifytime(provideID,oneSchedule).getId());

	}
	@Test
	@Order(5)
	public void test_deleteSchedule() {
		int provideID = 1;

		scheduleservices.deleteSchedule(provideID);
		verify(scheduledao, times(1)).deleteById(provideID);
	}

}
