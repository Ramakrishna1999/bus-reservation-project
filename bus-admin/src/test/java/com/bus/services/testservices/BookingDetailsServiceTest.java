package com.bus.services.testservices;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;

import com.admin.services.crudoperation.BookingService;
import com.admin.services.dao.BookingDao;
import com.admin.services.enums.PaymentStatus;
import com.admin.services.enums.Role;
import com.admin.services.models.BookingDetails;
import com.admin.services.models.Member;
import com.bus.services.testcontrollers.RouteControllerMockitoTest;

@TestMethodOrder(OrderAnnotation.class)
@SpringBootTest(classes = { RouteControllerMockitoTest.class })
public class BookingDetailsServiceMockitoTest {

	@Mock
	BookingDao bookingdao;

	@InjectMocks
	BookingService service;

	public List<BookingDetails> booking;

	@Test
	@Order(1)
	public void test_GetAllTicketDetails() {
		Member member = new Member(1L, "ram", "ram@gmail.com", Role.USER, "1525616661", "wttty122");
		booking = new ArrayList<BookingDetails>();
		booking.add(new BookingDetails(121111, 1, LocalDateTime.now(), 3, member, PaymentStatus.PAID, 2));
		when(bookingdao.findAll()).thenReturn(booking);
		assertEquals(booking, service.findAllTickets());
		
	}

	@Test
	@Order(2)
	public void test_GetSingleTicketDetails() {
		Member member = new Member(1L, "ram", "ram@gmail.com", Role.USER, "1525616661", "wttty122");

		BookingDetails booking = new BookingDetails(121111, 1, LocalDateTime.now(), 3, member, PaymentStatus.PAID, 2);
		long bookingid = 121111;

		when(bookingdao.findByBookingId(bookingid)).thenReturn(booking);
		assertEquals(booking, service.findTicket(bookingid));
	}

}
