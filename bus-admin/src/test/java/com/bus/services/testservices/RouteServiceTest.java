package com.bus.services.testservices;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;

import com.admin.services.crudoperation.RouteService;
import com.admin.services.dao.RouteDao;
import com.admin.services.models.Route;
@TestMethodOrder(OrderAnnotation.class)
@SpringBootTest(classes = { RouteServiceMockitoTest.class })
public class RouteServiceMockitoTest {

	@Mock
	RouteDao routedao;

	@InjectMocks
	RouteService routeservices;

	public List<Route> routes;

	public Route routeForOne;

	// test for find multiple routes
	@Test
	@Order(1)
	public void test_getFindRoutes() {
		routes = new ArrayList<Route>();
		routes.add(new Route(1, 111l, "Choppadandi", "Manchiryal", 150l));
		when(routedao.findAll()).thenReturn(routes);
		assertEquals(1, routeservices.findRoutes().size());
	}

	// test for find single route
	@Test
	@Order(2)
	public void test_getfindById() {
		Route routeFOrOne = new Route(1, 111l, "Choppadandi", "Manchiryal", 150l);
		int provideID = 1;
		when(routedao.findById(provideID)).thenReturn(routeFOrOne);
		assertEquals(provideID, routeservices.findRoute(provideID).getId());

	}

	// test for update single route
	@Test
	@Order(3)
	public void test_updateSingleRoute() {
		routeForOne = new Route(1, 111l, "Choppadandi", "Manchiryal", 150l);
		int provideID = 1;
		when(routedao.save(routeForOne)).thenReturn(routeForOne);
		assertEquals(routeForOne, routeservices.updateRoute(provideID, routeForOne));

	}

	// test for create a route
	@Test
	@Order(4)
	public void test_saveRoute() {
		routeForOne = new Route(1, 111l, "Choppadandi", "Manchiryal", 112l);
		when(routedao.save(routeForOne)).thenReturn(routeForOne);
		assertEquals(routeForOne, routeservices.createRoute(routeForOne));
	}

	// test for delete single route
	@Test
	@Order(5)
	public void test_deleteRoute() {

		Route routeForDelete = new Route(1, 111l, "Choppadandi", "Manchiryal", 180l);

		routeservices.deleteRoute(routeForDelete.getId());
		verify(routedao, times(1)).deleteById(routeForDelete.getId());
	}

}
