package com.bus.services.testcontrollers;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;

import com.admin.services.controller.RouteController;
import com.admin.services.crudoperation.RouteService;
import com.admin.services.models.Route;

@TestMethodOrder(OrderAnnotation.class)
@SpringBootTest(classes = { RouteControllerMockitoTest.class })
public class RouteControllerMockitoTest {

	@Mock
	RouteService services;

	@InjectMocks
	RouteController controller;

	public List<Route> routesList;

	@Test
	@Order(1)
	public void test_FindAllRoutes() {
		routesList = new ArrayList<Route>();
		routesList.add(new Route(1, 111L, "vemulawada", "Karimnagar", 99));
		routesList.add(new Route(2, 112L, "Jagityal", "Korutla", 79));
		when(services.findRoutes()).thenReturn(routesList);
		controller.getAllRoute();

	}

	@Test
	@Order(2)
	public void test_FindOneRoute() {

		Route oneRoute = new Route(1, 111L, "vemulawada", "Karimnagar", 99);
		int expected = 1;
		when(services.findRoute(expected)).thenReturn(oneRoute);
		assertEquals(expected, controller.getOneRoute(expected).getId());

	}

	@Test
	@Order(3)
	public void test_UpdateOneRoute() {

		Route route = new Route(1, 111L, "vemulawada", "Karimnagar", 99);
		int expected = 1;
		System.out.println(route.getId());
		when(services.updateRoute(expected, route)).thenReturn(route);
		assertEquals(expected, controller.modifyRoute(expected, route).getId());
	}

	@Test
	@Order(4)
	public void test_DeleteOneRoute() {

		Route route = new Route(1, 111L, "vemulawada", "Karimnagar", 99);
		int expected = 1;
		controller.deleteOneRoute(expected);
		System.out.println(route.getId());
		verify(services, times(1)).deleteRoute(expected);
	}
	@Test
	@Order(5)
	public void test_CreateRoute() {
		Route route = new Route(1, 111L, "vemulawada", "Karimnagar", 99);
		when(services.createRoute(route)).thenReturn(route);
		assertEquals(route, controller.createRoute(route));
	}

}
