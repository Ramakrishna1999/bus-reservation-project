package com.bus.services.testcontrollers;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;

import com.admin.services.controller.BusController;
import com.admin.services.crudoperation.BusService;
import com.admin.services.models.Bus;
import com.admin.services.models.Route;
import com.admin.services.models.Schedule;

@TestMethodOrder(OrderAnnotation.class)
@SpringBootTest(classes = { BusControllerMockitoTest.class })
public class BusControllerMockitoTest {

	@Mock
	BusService busservice;

	@InjectMocks
	BusController buscontroller;
	public List<Bus> buses;

	@Test
	@Order(1)
	public void test_BusFindAll() {

		Schedule schedule = new Schedule("07:00:00", "08:00:00", "01-01-2022", 1);

		Route route = new Route(1, 111L, "vemulawada", "Karimnagar", 99);

		buses = new ArrayList<Bus>();
		buses.add(new Bus(1, "ts02tt1616", "express", 2, route, schedule));
		when(busservice.findAllBusDetails()).thenReturn(buses);
		assertEquals(buses, buscontroller.getAllBus());

	}

	@Test
	@Order(2)
	public void test_FindOneBus() {
		Schedule schedule = new Schedule("07:00:00", "08:00:00", "01-01-2022", 1);

		int expected = 1;
		Route route = new Route(1, 111L, "vemulawada", "Karimnagar", 99);
		Bus bus = new Bus(1, "ts02tt1616", "express", 2, route, schedule);
		when(busservice.findBusDetails(expected)).thenReturn(bus);
		assertEquals(expected, buscontroller.getOneBus(expected).getId());
	}

	@Test
	@Order(3)
	public void test_UpdateOneBus() {
		Schedule schedule = new Schedule("07:00:00", "08:00:00", "01-01-2022", 1);

		int expected = 1;
		Route route = new Route(1, 111L, "vemulawada", "Karimnagar", 99);
		Bus bus = new Bus(1, "ts02tt1616", "express", 2, route, schedule);
		when(busservice.modifyBusDetails(expected, bus)).thenReturn(bus);
		assertEquals(expected, buscontroller.updateBus(expected, bus).getId());
	}

	@Test
	@Order(4)
	public void test_CreateOneBus() {
		Schedule schedule = new Schedule("07:00:00", "08:00:00", "01-01-2022", 1);

		Route route = new Route(1, 111L, "vemulawada", "Karimnagar", 99);
		Bus bus = new Bus(1, "ts02tt1616", "express", 2, route, schedule);
		when(busservice.createBus(bus)).thenReturn(bus);
		System.out.println(bus.getId());
		assertEquals(bus, buscontroller.addBus(bus));
	}

	@Test
	@Order(5)
	public void test_DeleteOneBus() {
		Schedule schedule = new Schedule("07:00:00", "08:00:00", "01-01-2022", 1);

		Route route = new Route(1, 111L, "vemulawada", "Karimnagar", 99);
		Bus bus = new Bus(1, "ts02tt1616", "express", 2, route, schedule);
		int expected = 1;
		buscontroller.deleteOneBus(expected);

		verify(busservice, times(1)).deleteBus(bus.getId());
	}

}
