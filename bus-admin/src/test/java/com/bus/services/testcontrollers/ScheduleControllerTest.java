package com.bus.services.testcontrollers;

import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.TestMethodOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;

import com.admin.services.controller.ScheduleController;
import com.admin.services.crudoperation.ScheduleService;
import com.admin.services.models.Schedule;

@TestMethodOrder(OrderAnnotation.class)
@SpringBootTest(classes = { ScheduleControllerMockitoTest.class })
public class ScheduleControllerMockitoTest {

	@Mock
	ScheduleService services;

	@InjectMocks
	ScheduleController controller;

	public List<Schedule> schedules;

	@Test
	@Order(1)
	public void test_FindAllSchedules() {

		List<Schedule> schedules = new ArrayList<Schedule>();
		Schedule schedule = new Schedule("07:00:00", "08:00:00", "01-01-2022", 1);

		when(services.findSchedules()).thenReturn(schedules);
		List<Schedule> afterRetriveSchedules = controller.findAllSchedule();
		assertEquals(schedules, afterRetriveSchedules);
	}

	@Test
	@Order(2)
	public void test_FindOneSchedule() {

		Schedule schedule = new Schedule("07:00:00", "08:00:00", "01-01-2022", 1);
		int expected = 1;
		when(services.findScheduleById(expected)).thenReturn(schedule);
		Schedule retrivedSchedule = controller.findScheduleThroughId(expected);

		assertEquals(expected, retrivedSchedule.getId());
	}

	@Test
	@Order(3)
	public void test_CreateOneSchedule() {

		Schedule schedule = new Schedule("07:00:00", "08:00:00", "01-01-2022", 1);

		when(services.createSchedule(schedule)).thenReturn(schedule);

		assertEquals(schedule, controller.scheduleCreation(schedule));
	}

	@Test
	@Order(4)
	public void test_UpdateOneSchedule() {

		Schedule schedule = new Schedule("07:00:00", "08:00:00", "01-01-2022", 1);
		int expected = 1;
		when(services.modifytime(expected, schedule)).thenReturn(schedule);
		Schedule afterUpdate = controller.updateSchedule(expected, schedule);
		assertEquals(expected, afterUpdate.getId());
	}

	@Test
	@Order(5)
	public void test_DeleteOneSchedule() {

		Schedule schedule = new Schedule("07:00:00", "08:00:00", "01-01-2022", 1);
		int expected = 1;

		controller.deleteOneSChedule(expected);

		verify(services, times(1)).deleteSchedule(schedule.getId());

	}

}
