package com.bus.services;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import com.bus.services.testcontrollers.RouteControllerMockitoTest;

@SpringBootTest(classes = { BusAdminApplicationTests.class })
class BusAdminApplicationTests {

	@Test
	void contextLoads() {
	}

}
