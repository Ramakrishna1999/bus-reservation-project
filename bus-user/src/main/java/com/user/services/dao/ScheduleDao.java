package com.user.services.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.user.services.models.Schedule;

@Repository
public interface ScheduleDao extends JpaRepository<Schedule, Integer> {

	List<Schedule> findById(int id);

	// @Query("select endTime,startTime from Schedule where dateOfJourney=?1")
	Schedule findByDateOfJourney(String date);

}
