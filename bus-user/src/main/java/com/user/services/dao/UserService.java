package com.user.services.dao;


import org.springframework.stereotype.Service;

import com.user.services.request.UserRequest;


@Service
public interface UserService {
	/**
	 * This Interface is used to convert Application User to CreateUserRequest
	 * @param createUserRequest
	 * @return
	 */
	UserRequest createUser(UserRequest createUserRequest);

}
