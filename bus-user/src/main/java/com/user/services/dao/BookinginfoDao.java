package com.user.services.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.user.services.models.BookingDetails;
import com.user.services.models.Member;

@Repository
public interface BookinginfoDao extends JpaRepository<BookingDetails, Long> {

	List<BookingDetails> findBymember(Member memberid);
	
	BookingDetails findById(long id);
	

}
