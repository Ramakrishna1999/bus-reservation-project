package com.user.services.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.user.services.models.Member;
import com.user.services.models.Passenger;

@Repository
public interface PassengerDao extends JpaRepository<Passenger, Long> {

	Passenger findById(long passengerid);

	Passenger findByName(String name);

	List<Passenger> findByMember(Member name);
	
}
