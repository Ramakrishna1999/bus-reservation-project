package com.user.services.dao;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.user.services.models.Member;

@Repository
public interface MemberDao extends JpaRepository<Member, Long> {

	// Member findByName(String username);
	Optional<Member> findByName(String name);

	Member findById(long id);

	//Member save(Member member);

}
