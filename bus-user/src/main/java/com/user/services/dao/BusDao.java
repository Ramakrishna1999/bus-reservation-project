package com.user.services.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.user.services.models.Bus;

@Repository
public interface BusDao extends JpaRepository<Bus, Integer> {

	List<Bus> findByRouteId(int id);

	 Bus findById(int id);
	// List<Bus> findByRouteno(Long routeno);

}
