package com.user.services.exceptions;

public class SeatsAreFilled extends Exception {

	public SeatsAreFilled(String message) {
		
		super(message);
	}

	
	private static final long serialVersionUID = 1L;

}
