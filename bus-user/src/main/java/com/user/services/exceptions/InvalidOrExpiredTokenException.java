package com.user.services.exceptions;

public class InvalidOrExpiredTokenException extends RuntimeException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	public String toString() {
		
		return "Invalid or expired jwt token";
	}
}
