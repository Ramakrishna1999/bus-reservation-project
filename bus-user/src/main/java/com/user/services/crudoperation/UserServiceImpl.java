package com.user.services.crudoperation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.user.services.dao.MemberDao;
import com.user.services.dao.UserService;
import com.user.services.enums.Role;
import com.user.services.models.Member;
import com.user.services.request.UserRequest;
@Service
public class UserServiceImpl implements UserService{

	@Autowired
	MemberDao memberDao;
	@Autowired
	private PasswordEncoder passwordEncoder;

	public UserServiceImpl(MemberDao memberDao) {
		super();
		this.memberDao = memberDao;
	}

	/**
	 * This method Is used to create the ApplicationUser based on CreateUserRequest
	 * Return CreateUserRequest instance
	 */
	@Override
	public UserRequest createUser(UserRequest createUserRequest) {
		Member user = new Member(1l,"shiva","shiva@gmail.com",Role.USER,"shiva123","8899189919");
		user.setName(createUserRequest.getUsername());
		user.setPassword(passwordEncoder.encode(createUserRequest.getPassword()));
		user.setRole(Role.USER);
		user = memberDao.save(user);
		return new UserRequest(user.getId(),user.getName(),user.getEmail(),user.getRole(),user.getPhone(),user.getPassword());
	}

}
