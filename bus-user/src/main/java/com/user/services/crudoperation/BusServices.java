package com.user.services.crudoperation;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.user.services.dao.BookinginfoDao;
import com.user.services.dao.BusDao;
import com.user.services.dao.MemberDao;
import com.user.services.dao.PassengerDao;
import com.user.services.dao.RouteDao;
import com.user.services.models.BookingDetails;
import com.user.services.models.Bus;
import com.user.services.models.Member;
import com.user.services.models.Passenger;
import com.user.services.models.Route;

@Service
public class BusServices {

	private static final Logger log = LoggerFactory.getLogger(BusServices.class);

	@Autowired
	private BusDao busDao;

	@Autowired
	private PassengerDao passengerDao;

	@Autowired
	private MemberDao memberDao;

	@Autowired
	private RouteDao routeDao;

	@Autowired
	private BookinginfoDao bookInfo;

	Route routeList = new Route();
	List<Bus> bus = new ArrayList<Bus>();

	/*
	 * it will take parameters from api then give route information to reference
	 * variable routelist
	 */
	public List<Bus> findingFromPlace(String source, String destination, String date) {
		Route routeAfterFetch = routeDao.findBySource(source);
		if (routeAfterFetch.getDestination().equals(destination)) {
			routeList = routeAfterFetch;
		} else {
			routeList = null;
		}
		List<Bus> buses = new ArrayList<Bus>();
		buses = busDao.findByRouteId(routeList.getId());

		for (Bus b : buses) {

			if (b.getSchedule().getDateOfJourney().equals(date)) {
				bus = buses;
			} else {
				bus = null;
			}
		}
		return bus;

	}

	public Passenger sendingPassenger(Passenger passenger) {
		return passengerDao.save(passenger);

	}

	public Member sendingMember(Member member) {
		Member members = memberDao.save(member);

		return members;
	}

	public BookingDetails generateTicket(BookingDetails booking) {
		Bus buses = busDao.findById(booking.getBusid());
		int record = buses.getAvailableSeats();
		if (booking.getSeats() <= record) {
			Route route = buses.getRoute();
			long fare = route.getCost() * booking.getSeats();
			buses.setAvailableSeats(buses.getAvailableSeats() - booking.getSeats());
			booking.setFare(fare);
			bookInfo.save(booking);
		} else {

			log.info("Seats are not available");
		}

		return booking;

	}

	public List<Passenger> retriveTicketBasedOnUserId(long memberid) {

		Member mem = memberDao.findById(memberid);

		return passengerDao.findByMember(mem);

	}

	public Passenger modifyPassenger(long id, String name) {

		Passenger afterFetch = passengerDao.findById(id);

		return afterFetch;

	}

	public Member changeMemberDetails(long id, String name) {

		Member memberAfterFetch = memberDao.findById(id);

		memberAfterFetch.setName(name);
		memberDao.save(memberAfterFetch);

		return memberAfterFetch;
	}

}
