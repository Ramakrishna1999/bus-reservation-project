package com.user.services.config;

import java.util.Arrays;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.user.services.dao.MemberDao;
import com.user.services.models.Member;

@Service
public class CustomUserDetailsService implements UserDetailsService {

	@Autowired
	MemberDao memberDao;

	/**
	 * 
	 * 
	 * To get users from user repository based on Username
	 */
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		List<SimpleGrantedAuthority> roles = null;
		Optional<Member> applicationUser = memberDao.findByName(username);
		if (applicationUser.isPresent()) {
			Member user = applicationUser.get();
			roles = Arrays.asList(new SimpleGrantedAuthority("ROLE_" + user.getRole().toString()));
			return new User(user.getName(), user.getPassword(), roles);
		}
		else {
		throw new UsernameNotFoundException("User not found with the Username " + username);
		}
	}

}
