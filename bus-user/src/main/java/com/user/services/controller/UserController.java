package com.user.services.controller;

import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.user.services.crudoperation.BusServices;
import com.user.services.exceptions.DataNotFoundException;
import com.user.services.exceptions.UserNotFoundException;
import com.user.services.models.BookingDetails;
import com.user.services.models.Bus;
import com.user.services.models.Member;
import com.user.services.models.Passenger;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping("/user")
public class UserController {

	@Autowired
	private BusServices busServices;

	@ApiOperation(value = "Search Busses")
	@ApiResponses(value = {
			@ApiResponse(code = HttpServletResponse.SC_OK, response = Bus.class, message = "Bus details are retrived succesfully"),
			@ApiResponse(code = HttpServletResponse.SC_BAD_REQUEST, response = String.class, message = "Invalid parameters"),
			@ApiResponse(code = HttpServletResponse.SC_FORBIDDEN, response = String.class, message = "UnAuthorized Access") })
	@GetMapping("/search/{source}/{destination}/{date}")
	public List<Bus> findBySourceAndDestination(@PathVariable("source") String source,
			@PathVariable("destination") String destination, @PathVariable("date") String date) {

		List<Bus> info = busServices.findingFromPlace(source, destination, date);
		if (info == null) {
			throw new DataNotFoundException("Entered values are Not Valid");
		}

		return info;

	}

	@ApiOperation(value = "Giving Passenger Details")
	@ApiResponses(value = {
			@ApiResponse(code = HttpServletResponse.SC_OK, response = String.class, message = "Passenger details are created succesfully"),
			@ApiResponse(code = HttpServletResponse.SC_BAD_REQUEST, response = String.class, message = "Invalid parameters"),
			@ApiResponse(code = HttpServletResponse.SC_FORBIDDEN, response = String.class, message = "UnAuthorized Access") })
	@PostMapping(value = "/createPassenger")
	public Passenger sendPassengerDetails(@RequestBody Passenger passenger) {

		return busServices.sendingPassenger(passenger);
	}

	@ApiOperation(value = "booking Ticket")
	@ApiResponses(value = {
			@ApiResponse(code = HttpServletResponse.SC_OK, response = String.class, message = "Passenger details are created succesfully"),
			@ApiResponse(code = HttpServletResponse.SC_BAD_REQUEST, response = String.class, message = "Invalid parameters"),
			@ApiResponse(code = HttpServletResponse.SC_FORBIDDEN, response = String.class, message = "UnAuthorized Access") })
	@PostMapping(value = "/booking")
	public BookingDetails sendbookingDetails(@RequestBody BookingDetails booking) {
		return busServices.generateTicket(booking);
	}

	@GetMapping(value = "/fetchticket/{userid}")
	@ApiResponses(value = {
			@ApiResponse(code = HttpServletResponse.SC_OK, response = String.class, message = "Ticket details are retrived succesfully"),
			@ApiResponse(code = HttpServletResponse.SC_BAD_REQUEST, response = String.class, message = "Invalid parameters"),
			@ApiResponse(code = HttpServletResponse.SC_FORBIDDEN, response = String.class, message = "UnAuthorized Access") })
	public List<Passenger> findTicketInformation(@RequestParam("userid") long memberid) {
		List<Passenger> ticketsAfterFetch = busServices.retriveTicketBasedOnUserId(memberid);

		if (ticketsAfterFetch == null) {
			throw new UserNotFoundException("Entered User is not found.");
		}

		return ticketsAfterFetch;
	}

	@PostMapping(value = "/membercreate")
	@ApiResponses(value = {
			@ApiResponse(code = HttpServletResponse.SC_OK, response = String.class, message = "Member details are created succesfully"),
			@ApiResponse(code = HttpServletResponse.SC_BAD_REQUEST, response = String.class, message = "Invalid parameters"),
			@ApiResponse(code = HttpServletResponse.SC_FORBIDDEN, response = String.class, message = "UnAuthorized Access") })
	public Member memberRegister(@RequestBody Member member) {
		return busServices.sendingMember(member);
	}

	@PutMapping(value = "/memberUpdate/{id}/{name}")
	@ApiResponses(value = {
			@ApiResponse(code = HttpServletResponse.SC_OK, response = String.class, message = "Member details are created succesfully"),
			@ApiResponse(code = HttpServletResponse.SC_BAD_REQUEST, response = String.class, message = "Invalid parameters"),
			@ApiResponse(code = HttpServletResponse.SC_FORBIDDEN, response = String.class, message = "UnAuthorized Access") })
	public Member modifyMemberDetails(@PathVariable("id") long id, @PathVariable("name") String name) {
		Member afterFetchMemeber = busServices.changeMemberDetails(id, name);
		if (afterFetchMemeber == null) {
			throw new UserNotFoundException("Entered User is not found.");
		}

		return afterFetchMemeber;
	}

	@PutMapping("/passengerUpdate/{id}/{name}")
	public Passenger changePassengerDetails(@PathVariable("id") long id, @PathVariable("name") String name) {
		Passenger afterFetch = busServices.modifyPassenger(id, name);
		if (afterFetch == null) {
			throw new UserNotFoundException("Entered Passenger is not found.");
		}
		return afterFetch;
	}
}
