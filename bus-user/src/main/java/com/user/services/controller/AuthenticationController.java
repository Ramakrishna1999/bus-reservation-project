package com.user.services.controller;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.user.services.config.CustomUserDetailsService;
import com.user.services.config.JwtTokenUtil;
import com.user.services.exceptions.Constants;
import com.user.services.request.JwtRequest;
import com.user.services.response.JWTTokenResponse;

import io.swagger.annotations.Api;

@RestController
@RequestMapping(value = "/")
@Api(value = "User Login ", tags = { "User Login" })
@Validated
public class AuthenticationController {

	@Autowired
	private AuthenticationManager authenticationManager;

	@Autowired
	private JwtTokenUtil jwtTokenUtil;

	@Autowired
	private CustomUserDetailsService userDetailsService;

	@RequestMapping(value = "/auth", method = RequestMethod.POST)
	public ResponseEntity<?> login(@RequestBody JwtRequest authenticationRequest) throws AccessDeniedException {
		authenticate(authenticationRequest.getName(), authenticationRequest.getPassword());
		final UserDetails userDetails = userDetailsService.loadUserByUsername(authenticationRequest.getName());
		final String token = jwtTokenUtil.generateToken(userDetails);
		return ResponseEntity.ok(new JWTTokenResponse(token, "Bearer", Constants.JWT_TOKEN_VALIDITY));
	}

	private void authenticate(String name, String password) throws AccessDeniedException {
		try {
			authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(name, password));
		} catch (DisabledException e) {
			throw new AccessDeniedException("USER_DISABLED", e);
		} catch (BadCredentialsException e) {
			throw new AccessDeniedException("INVALID_CREDENTIALS", e);
		}
	}

}
