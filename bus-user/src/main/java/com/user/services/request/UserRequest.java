package com.user.services.request;


import org.springframework.stereotype.Service;


import com.user.services.enums.Role;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.NotBlank;

@Service
public class UserRequest {
	private long id;
	private String email;
	private String phone;
	@NotBlank(message = "Username should not be null or Empty")
	private String name;
	@NotBlank(message = "Password should not be null or Empty")
	private String password;
	@NotBlank(message = "Role should not be null or Empty")
	@Enumerated(EnumType.STRING)
	private Role role;

	public UserRequest(Long id, String name, String email, Role role, String phone, String password) {
		super();
		this.name = name;
		this.password = password;
		this.role = role;
		this.email=email;
		this.phone=phone;
		this.id=id;
		
	}

	

	public UserRequest() {
		super();
	}

	public String getUsername() {
		return name;
	}

	public void setUsername(String username) {
		this.name = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}



	public long getId() {
		return id;
	}



	public void setId(long id) {
		this.id = id;
	}



	public String getEmail() {
		return email;
	}



	public void setEmail(String email) {
		this.email = email;
	}



	public String getPhone() {
		return phone;
	}



	public void setPhone(String phone) {
		this.phone = phone;
	}
	

}


