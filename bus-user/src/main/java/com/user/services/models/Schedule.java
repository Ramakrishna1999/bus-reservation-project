package com.user.services.models;

import javax.persistence.Entity;
import javax.persistence.Id;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonFormat.Shape;

@Entity
public class Schedule {

//	@JsonIgnore
//	@DateTimeFormat(iso = DateTimeFormat.ISO.TIME)
//	@JsonSerialize(using = LocalTimeSerializer.class)
//	@JsonDeserialize(using = LocalTimeDeserializer.class)
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "HH:mm:ss")
	
	private String startTime;

//	@JsonIgnore
//	@DateTimeFormat(iso = DateTimeFormat.ISO.TIME)
//	@JsonSerialize(using = LocalTimeSerializer.class)
//	@JsonDeserialize(using = LocalTimeDeserializer.class)
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "HH:mm:ss")
	private String endTime;

	@JsonFormat(pattern = "DD-MM-yyyy", shape = Shape.STRING)
	private String dateOfJourney;
	@Id
	private int id;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDateOfJourney() {
		return dateOfJourney;
	}

	public void setDateOfJourney(String dateOfJourney) {
		this.dateOfJourney = dateOfJourney;
	}

	public String getStartTime() {
		return startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	public String getEndTime() {
		return endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	
	public Schedule(String startTime, String endTime, String dateOfJourney, int id) {
		super();
		this.startTime = startTime;
		this.endTime = endTime;
		this.dateOfJourney = dateOfJourney;
		this.id = id;
	}

	public Schedule() {
		super();
		// TODO Auto-generated constructor stub
	}

	

}
