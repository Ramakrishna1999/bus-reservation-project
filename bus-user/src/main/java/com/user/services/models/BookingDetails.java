package com.user.services.models;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import com.user.services.enums.PaymentStatus;

@Entity
public class BookingDetails {

	@Id
	@Column(name = "booking_id")
	private long bookingId;

	private int busid;
	private LocalDateTime bookedtime;
	private int seats;

	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "member_id")
	private Member member;
	@Enumerated(EnumType.STRING)
	private PaymentStatus paymentstatus;
	private long fare;

	public long getBookingId() {
		return bookingId;
	}

	public void setBookingId(long bookingId) {
		this.bookingId = GenerateReferenceNo.generateRef();
	}

	public int getBusid() {
		return busid;
	}

	public void setBusid(int busid) {
		this.busid = busid;
	}

	public LocalDateTime getBookedtime() {
		return bookedtime;
	}

	public void setBookedtime(LocalDateTime bookedtime) {
		this.bookedtime = bookedtime;
	}

	public int getSeats() {
		return seats;
	}

	public void setSeats(int seats) {
		this.seats = seats;
	}

	

	public Member getMember() {
		return member;
	}

	public void setMember(Member member) {
		this.member = member;
	}

	public PaymentStatus getPaymentstatus() {
		return paymentstatus;
	}

	public void setPaymentstatus(PaymentStatus paymentstatus) {
		this.paymentstatus = paymentstatus;
	}

	public long getFare() {
		return fare;
	}

	public void setFare(long fare) {
		this.fare = fare;
	}

	public BookingDetails() {
		super();
		
	}

	public BookingDetails(long bookingId, int busid, LocalDateTime bookedtime, int seats, Member member,
			PaymentStatus paymentstatus, long fare) {
		super();
		this.bookingId = bookingId;
		this.busid = busid;
		this.bookedtime = bookedtime;
		this.seats = seats;
		this.member = member;
		this.paymentstatus = paymentstatus;
		this.fare = fare;
	}
	
}
