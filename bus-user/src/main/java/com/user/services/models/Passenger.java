package com.user.services.models;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.user.services.enums.Gender;

@Entity
public class Passenger {

	@Id
	private long id;
	private int passengerid;
	private String name;
	@Enumerated(EnumType.STRING)
	private Gender gender;
	private int age;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "Member_id")
	Member member;

	/**
	 * @return the name
	 */

	public String getName() {
		return name;
	}

	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	public Passenger() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @return the gender
	 */
	public Gender getGender() {
		return gender;
	}

	/**
	 * @param gender the gender to set
	 */
	public void setGender(Gender gender) {
		this.gender = gender;
	}

	/**
	 * @return the age
	 */
	public int getAge() {
		return age;
	}

	/**
	 * @param age the age to set
	 */
	public void setAge(int age) {
		this.age = age;
	}

	/**
	 * @return the passengerid
	 */
	public int getPassengerid() {
		return passengerid;
	}

	/**
	 * @param passengerid the passengerid to set
	 */
	public void setPassengerid(int passengerid) {
		this.passengerid = passengerid;
	}

	public Passenger(long id, int passengerid, String name, Gender gender, int age, Member member) {
		super();
		this.id = id;
		this.passengerid = passengerid;
		this.name = name;
		this.gender = gender;
		this.age = age;
		this.member = member;
	}

	public Member getMember() {
		return member;
	}

	public void setMember(Member member) {
		this.member = member;
	}

	

	
}
