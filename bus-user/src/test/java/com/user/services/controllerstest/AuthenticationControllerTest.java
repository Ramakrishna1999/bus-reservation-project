package com.user.services.controllerstest;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.test.web.servlet.MockMvc;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.user.services.config.CustomUserDetailsService;
import com.user.services.config.JwtAuthenticationEntryPoint;
import com.user.services.config.JwtTokenUtil;
import com.user.services.controller.UserController;
import com.user.services.crudoperation.BusServices;
import com.user.services.enums.Role;
import com.user.services.models.Member;
import com.user.services.request.JwtRequest;
import com.user.services.request.UserRequest;
import com.user.services.response.JwtResponse;
import com.user.services.response.UserResponse;

@WebMvcTest(value = UserController.class)
@ExtendWith(MockitoExtension.class)

public class AuthenticationControllerTest {

	Logger logger = LoggerFactory.getLogger(AuthenticationControllerTest.class);

//	@InjectMocks
//	AuthenticationController authController;
	@Autowired
	private MockMvc mockMvc;

	@MockBean
	private BusServices userCommandService;

	@MockBean
	private JwtTokenUtil jwtTokenUtil;
	@MockBean
	CustomUserDetailsService customUserDetailsService;

	@MockBean
	JwtAuthenticationEntryPoint jwtAuthenticationEntryPoint;
	@MockBean
	AuthenticationManager authenticationManager;

	private static UserDetails usedetails;
	private static String jwtToken;

	@BeforeEach
	public void setUp() {
		usedetails = new User("admin", "admin", new ArrayList<>());
		jwtToken = jwtTokenUtil.generateToken(usedetails);
	}

	@Test
	@Order(1)
	public void testLoginReturnsJwt() throws Exception {
		JwtRequest authenticationRequest = new JwtRequest("admin", "admin");
		
		JwtResponse authenticationResponse = new JwtResponse("anyToken");
		String jsonCreate = new Gson().toJson(authenticationRequest);
		String jsonResponse = new Gson().toJson(authenticationResponse);
		when(jwtTokenUtil.generateToken(usedetails)).thenReturn("anyToken");
		when(customUserDetailsService.loadUserByUsername(authenticationRequest.getName())).thenReturn(usedetails);
		ObjectMapper objectMapper = new ObjectMapper();
		String jsonBody = objectMapper.writeValueAsString(authenticationRequest);
		this.mockMvc
				.perform(post("/user/login").content(jsonBody).content(jsonCreate).content(jsonResponse)
						.contentType(MediaType.APPLICATION_JSON_VALUE).accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk()).andDo(print());

	}

	@Test
	@Order(2)
	public void testCreateUser() throws Exception {
		UserRequest createUserRequest = new UserRequest(1l,"shiva","shiva@gmail.com",Role.USER,"9910001009", "admin");
		UserResponse userResponse = new UserResponse("admin", "admin");
		Member member = new Member(1l, "shiva", "shiva@gmail.com", Role.USER, "99891999919", "ran111");

		String jsonCreate = new Gson().toJson(createUserRequest);
		String jsonResponse = new Gson().toJson(userResponse);
		when(customUserDetailsService.loadUserByUsername(createUserRequest.getUsername())).thenReturn(usedetails);
		when(userCommandService.sendingMember(member)).thenReturn(member);
		ObjectMapper objectMapper = new ObjectMapper();
		String jsonBody = objectMapper.writeValueAsString(createUserRequest);
		this.mockMvc.perform(post("/admin/users").header("Authorization", "Bearer " + jwtToken).content(jsonBody)
				.content(jsonCreate).content(jsonResponse).contentType(MediaType.APPLICATION_JSON_VALUE)
				.accept(MediaType.APPLICATION_JSON)).andExpect(status().isOk()).andDo(print());

	}

}
