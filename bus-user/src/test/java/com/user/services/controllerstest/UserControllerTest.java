package com.user.services.controllerstest;

import static org.junit.Assert.assertThrows;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;

import com.user.services.controller.UserController;
import com.user.services.crudoperation.BusServices;
import com.user.services.enums.Gender;
import com.user.services.enums.PaymentStatus;
import com.user.services.enums.Role;
import com.user.services.exceptions.DataNotFoundException;
import com.user.services.exceptions.UserNotFoundException;
import com.user.services.models.BookingDetails;
import com.user.services.models.Bus;
import com.user.services.models.GenerateReferenceNo;
import com.user.services.models.Member;
import com.user.services.models.Passenger;
import com.user.services.models.Route;
import com.user.services.models.Schedule;

@TestMethodOrder(OrderAnnotation.class)
@SpringBootTest(classes = { UserControllerTest.class })
public class UserControllerTest {

	@Mock
	BusServices busServices;

	@InjectMocks
	UserController userController;

	public List<Route> routes;

	public List<Bus> busForFetch;

	@Test
	@Order(1)
	public void test_findBySourceAndDestination() {
		busForFetch = new ArrayList<Bus>();
		Route route = new Route(1, 111l, "Choppadandi", "Manchiryal", 112l);
		Schedule schedule = new Schedule("08:00:00", "09:00:00", "01-01-2022", 1);
		Bus bus1 = new Bus(1, "ts02tt1616", "express", 2, route, schedule);
		Bus bus2 = new Bus(2, "ts02tt1613", "express", 3, route, schedule);
		busForFetch.add(bus1);
		busForFetch.add(bus2);
		String source = "Choppadandi";
		String destination = "Manchiryal";
		String date = "01-01-2022";
		System.out.println(busForFetch);
		when(busServices.findingFromPlace(source, destination, date)).thenReturn(busForFetch);
		assertEquals(2, userController.findBySourceAndDestination(source, destination, date).size());

	}

	@Test
	@Order(2)
	public void test_findTicketInformation() {
		Member member = new Member(1l, "shiva", "shiva@gmail.com", Role.USER, "99891999919", "ran111");
		List<Passenger> passengers = new ArrayList<Passenger>();

		List<BookingDetails> booking = new ArrayList<BookingDetails>();
		booking.add(new BookingDetails(GenerateReferenceNo.generateRef(), 1, LocalDateTime.now(), 2, member,
				PaymentStatus.PAID, 120l));
		long memberid = 1l;
		when(busServices.retriveTicketBasedOnUserId(memberid)).thenReturn(passengers);
		assertEquals(passengers, userController.findTicketInformation(memberid));

	}

	@Test
	@Order(3)
	public void test_createPassenger() {
		Member member = new Member(1L, "shiva", "shiva@gmail.com", Role.USER, "99891999919", "ran111");

		Passenger passenger = new Passenger(1L, 1111, "devansh", Gender.MALE, 23, member);

		when(busServices.sendingPassenger(passenger)).thenReturn(passenger);
		assertEquals(passenger, userController.sendPassengerDetails(passenger));

	}

	@Test
	@Order(4)
	public void test_createMember() {
		Member member = new Member(1L, "shiva", "shiva@gmail.com", Role.USER, "99891999919", "ran111");
		when(busServices.sendingMember(member)).thenReturn(member);
		assertEquals(member, userController.memberRegister(member));
	}

	@Test
	@Order(5)
	public void test_modifyMemberDetails() {
		Member member = new Member(1L, "shiva", "shiva@gmail.com", Role.USER, "99891999919", "ran111");

		long id = 1L;
		String name = "shiva";
		when(busServices.changeMemberDetails(id, name)).thenReturn(member);
		assertEquals(member, userController.modifyMemberDetails(id, name));

	}

	@Test
	@Order(6)
	public void test_sendbookingDetails() {
		Member member = new Member(1L, "shiva", "shiva@gmail.com", Role.USER, "99891999919", "ran111");
		LocalDateTime localDateTime = LocalDateTime.now();
		Route route = new Route(1, 111l, "Choppadandi", "Manchiryal", 112l);
		Schedule schedule = new Schedule("08:00:00", "09:00:00", "01-01-2022", 1);
		Bus bus1 = new Bus(1, "ts02tt1616", "express", 2, route, schedule);

		BookingDetails booking = new BookingDetails(GenerateReferenceNo.generateRef(), bus1.getId(), localDateTime, 2,
				member, PaymentStatus.PAID, 224l);
		when(busServices.generateTicket(booking)).thenReturn(booking);
		int busid = 1;
		long fare = 224l;
		assertEquals(localDateTime, userController.sendbookingDetails(booking).getBookedtime());
		assertEquals(busid, userController.sendbookingDetails(booking).getBusid());
		assertEquals(fare, userController.sendbookingDetails(booking).getFare());
		PaymentStatus status = PaymentStatus.PAID;
		assertEquals(status, userController.sendbookingDetails(booking).getPaymentstatus());
		int seats = 2;
		assertEquals(seats, userController.sendbookingDetails(booking).getSeats());

	}

	@Test
	@Order(7)
	public void test_getModifyPassengerWithCustomException() {
		long memberid = 2l;
		String name = "shiva";
		when(busServices.modifyPassenger(memberid, name)).thenReturn(null);

		Exception exception = assertThrows(UserNotFoundException.class,
				() -> userController.changePassengerDetails(memberid, name));
		assertEquals("Entered Passenger is not found.", exception.getMessage());
	}

	@Test
	@Order(8)
	public void test_findTicketInformationIdWithUserNotFoundException() {
		long memberid = 2l;
		when(busServices.retriveTicketBasedOnUserId(memberid)).thenReturn(null);
		Exception exception = assertThrows(UserNotFoundException.class,
				() -> userController.findTicketInformation(memberid));
		assertEquals("Entered User is not found.", exception.getMessage());
	}

	@Test
	@Order(9)
	public void test_findBySourceWithDataNotFoundException() {
		String source = "Choppadandi";
		String destination = "Manchiryal";
		String date = "01-01-2022";
		when(busServices.findingFromPlace(source, destination, date)).thenReturn(null);
		Exception exception = assertThrows(DataNotFoundException.class,
				() -> userController.findBySourceAndDestination(source, destination, date));
		assertEquals("Entered values are Not Valid", exception.getMessage());

	}

}
