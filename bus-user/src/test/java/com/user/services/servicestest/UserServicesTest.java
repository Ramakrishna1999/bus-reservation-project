package com.user.services.servicestest;

import static org.junit.Assert.assertThrows;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;

import com.user.services.crudoperation.BusServices;
import com.user.services.dao.BookinginfoDao;
import com.user.services.dao.BusDao;
import com.user.services.dao.MemberDao;
import com.user.services.dao.PassengerDao;
import com.user.services.dao.RouteDao;
import com.user.services.dao.ScheduleDao;
import com.user.services.enums.Gender;
import com.user.services.enums.PaymentStatus;
import com.user.services.enums.Role;
import com.user.services.exceptions.UserNotFoundException;
import com.user.services.models.BookingDetails;
import com.user.services.models.Bus;
import com.user.services.models.GenerateReferenceNo;
import com.user.services.models.Member;
import com.user.services.models.Passenger;
import com.user.services.models.Route;
import com.user.services.models.Schedule;

@TestMethodOrder(OrderAnnotation.class)
@SpringBootTest(classes = { UserServicesTest.class })
public class UserServicesTest {

	@Mock
	RouteDao routeDao;

	@Mock
	BusDao busDao;

	@Mock
	MemberDao memberDao;

	@Mock
	ScheduleDao scheduleDao;

	@Mock
	BookinginfoDao bookingDao;

	@Mock
	PassengerDao passengerDao;

	@InjectMocks
	BusServices busService;

	@InjectMocks
	BookingDetails bookingDetails;

	public List<Route> routes;

	public List<Bus> busForFetch;

	@Test
	@Order(1)
	public void test_searchingBuses() {
		List<Bus> busForFetch = new ArrayList<Bus>();

		Route route = new Route(1, 111l, "Choppadandi", "Manchiryal", 112l);
		Schedule schedule = new Schedule("08:00:00", "09:00:00", "01-01-2022", 1);
		Bus bus1 = new Bus(1, "ts02tt1616", "express", 2, route, schedule);
		Bus bus2 = new Bus(2, "ts02tt1613", "express", 3, route, schedule);
		busForFetch.add(bus1);
		busForFetch.add(bus2);

		String source = "Choppadandi";
		String destination = "Manchiryal";
		String date = "01-01-2022";
		int routeid = 1;
		when(routeDao.findBySource(source)).thenReturn(route);
		when(scheduleDao.findByDateOfJourney(date)).thenReturn(schedule);
		when(busDao.findByRouteId(routeid)).thenReturn(busForFetch);

		assertEquals(2, busService.findingFromPlace(source, destination, date).size());
		assertEquals(2, busService.findingFromPlace(source, destination, date).get(0).getAvailableSeats());
		assertEquals("express", busService.findingFromPlace(source, destination, date).get(0).getBustype());
		assertEquals("ts02tt1616", busService.findingFromPlace(source, destination, date).get(0).getBusno());
		assertEquals(112l, busService.findingFromPlace(source, destination, date).get(0).getRoute().getCost());
		assertEquals("Choppadandi",
				busService.findingFromPlace(source, destination, date).get(0).getRoute().getSource());
		assertEquals(111l, busService.findingFromPlace(source, destination, date).get(0).getRoute().getRouteno());
		assertEquals("08:00:00",
				busService.findingFromPlace(source, destination, date).get(0).getSchedule().getStartTime());
		assertEquals("09:00:00",
				busService.findingFromPlace(source, destination, date).get(0).getSchedule().getEndTime());
		assertEquals(1, busService.findingFromPlace(source, destination, date).get(0).getSchedule().getId());

	}

	@Test
	@Order(2)
	public void test_getTicketByMemberid() {

		Member member = new Member(1L, "shiva", "shiva@gmail.com", Role.USER, "99891999919", "ran111");
		List<Passenger> passengers = new ArrayList<Passenger>();
		passengers.add(new Passenger(1L, 1111, "devansh", Gender.MALE, 23, member));
		passengers.add(new Passenger(2L, 1111, "srithej", Gender.MALE, 22, member));

		List<BookingDetails> booking = new ArrayList<BookingDetails>();
		booking.add(new BookingDetails(GenerateReferenceNo.generateRef(), 1, LocalDateTime.now(), 2, member,
				PaymentStatus.PAID, 120L));
		booking.add(new BookingDetails(GenerateReferenceNo.generateRef(), 2, LocalDateTime.now(), 2, member,
				PaymentStatus.PAID, 120L));
		long id = 1L;
		when(memberDao.findById(id)).thenReturn(member);

		when(passengerDao.findByMember(member)).thenReturn(passengers);
		long memberid = 1L;
		assertEquals(2, busService.retriveTicketBasedOnUserId(memberid).size());
		assertEquals(member, busService.retriveTicketBasedOnUserId(memberid).get(0).getMember());

	}

	@Test
	@Order(3)
	public void test_GenerateTicket() {
		Route route = new Route(1, 111l, "Choppadandi", "Manchiryal", 112l);
		Schedule schedule = new Schedule("08:00:00", "09:00:00", "01-01-2022", 1);
		Bus bus = new Bus(1, "TS01oo7181", "express", 4, route, schedule);
		Member member = new Member(1l, "shiva", "shiva@gmail.com", Role.USER, "99891999919", "ran111");

		BookingDetails bookingdetails = new BookingDetails(111111, 1, LocalDateTime.now(), 2, member,
				PaymentStatus.PAID, 120l);

		when(busDao.findById(bookingdetails.getBusid())).thenReturn(bus);
		when(bookingDao.save(bookingdetails)).thenReturn(bookingdetails);

		long bookid = 111111;
		assertEquals(bookid, busService.generateTicket(bookingdetails).getBookingId());
	}

	@Test
	@Order(4)
	public void test_sendingOnePassenger() {
		Member member = new Member(1L, "shiva", "shiva@gmail.com", Role.USER, "99891999919", "ran111");

		Passenger passenger = new Passenger(1L, 11, "devansh", Gender.MALE, 23, member);

		when(passengerDao.save(passenger)).thenReturn(passenger);

		long id = 1L;
		assertEquals(id, busService.sendingPassenger(passenger).getId());
		assertEquals(23, busService.sendingPassenger(passenger).getAge());
		assertEquals(Gender.MALE, busService.sendingPassenger(passenger).getGender());
		assertEquals("devansh", busService.sendingPassenger(passenger).getName());
		assertEquals(11, busService.sendingPassenger(passenger).getPassengerid());
		assertEquals(member, busService.sendingPassenger(passenger).getMember());
	}

	@Test
	@Order(5)
	public void test_sendingMember() {

		Member member = new Member(1l, "shiva", "shiva@gmail.com", Role.USER, "99891999919", "ran111");
		String email = "shiva@gmail.com";
		String name = "shiva";
		Role role = Role.USER;
		String pwd = "ran111";
		when(memberDao.save(member)).thenReturn(member);
		assertEquals(member, busService.sendingMember(member));
		assertEquals(email, busService.sendingMember(member).getEmail());
		assertEquals(member.getId(), busService.sendingMember(member).getId());
		assertEquals(name, busService.sendingMember(member).getName());
		assertEquals(role, busService.sendingMember(member).getRole());
		assertEquals(pwd, busService.sendingMember(member).getPassword());
		assertEquals("99891999919", busService.sendingMember(member).getPhone());

	}

	@Test
	@Order(6)
	public void test_modifyDetails() {
		Member member = new Member(1l, "shiva", "shiva@gmail.com", Role.USER, "99891999919", "ran111");
		long memberid = 1l;
		String name = "shiva";
		// long memberid1 = 2l;
		when(memberDao.findById(memberid)).thenReturn(member);
		when(memberDao.save(member)).thenReturn(member);
		assertEquals(name, busService.sendingMember(member).getName());

	}

	
	
	@Test
	@Order(9)
	public void test_setName() {
		Member member = new Member(1L, "shiva", "shiva@gmail.com", Role.USER, "99891999919", "ran111");

		Passenger passngr = new Passenger(1L, 1111, "devansh", Gender.MALE, 23, member);
		String name = "shiva";
		passngr.setName(name);
		assertEquals(name, passngr.getName());

	}

}
