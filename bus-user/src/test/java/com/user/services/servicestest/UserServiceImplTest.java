package com.user.services.servicestest;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import com.user.services.config.CustomUserDetailsService;
import com.user.services.config.JwtAuthenticationEntryPoint;
import com.user.services.config.JwtTokenUtil;
import com.user.services.dao.MemberDao;
import com.user.services.dao.UserService;
import com.user.services.enums.Role;
import com.user.services.models.Member;
import com.user.services.request.UserRequest;

@WebMvcTest(UserService.class)
public class UserServiceImplTest {

	@MockBean
	MemberDao memberDao;

	@Autowired
	//@MockBean
	private UserService userService;

	@MockBean
	private CustomUserDetailsService customUserDetailsService;

	@MockBean
	private JwtTokenUtil jwtUtil;

	@MockBean
	JwtAuthenticationEntryPoint jwtAuthenticationEntryPoint;

	@Test
	void testCreateUser() {
		Member user = new Member(1L, "shiva", "shiva@gmail.com", Role.USER, "7788198877", "shiva123");
		UserRequest createdUserRequest = new UserRequest(1l,"shiva","shiva@gmail.com", Role.USER,"9918881991", "shiva123");
		when(memberDao.save(any(Member.class))).thenReturn(user);
		
		createdUserRequest = userService.createUser(createdUserRequest);
		assertNotNull(createdUserRequest);
		assertEquals(user.getName(), createdUserRequest.getUsername());
		assertEquals(user.getPassword(), createdUserRequest.getPassword());

	}

}
